# No Shared Runners

### Disable shared Runners

Go to Settings > CI/CD > Expand Runners section > Disable shared Runners

### Install GitLab runner service in Ubuntu

First option with package manager (safer, but might install an older version)
`export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runne`

Second option, downloads latest version of script and runs it, considered less safe as it is easy to run something malicious:
`url -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash`

### Register a runner for your project/group

Run the register command and follow the instructions:
`sudo gitlab-runner register`
 - for gitlab-ci coordinator URL set: "https://gitlab.com/"
 - for gitlab-ci token, go to your project Settings > CI/CD > Expand Runners section > Use the registration token
 - use any name you want for your runner
 - don't set any tags
 - for the executor this time we will use ```shell```

After installation, start the runner service, which should start all your registered runners:
`sudo gitlab-runner run`

Go to your project Settings > CI/CD > Expand Runners section > See the newly registered runner under _Available specific runners_

For now you will be able to see how the runner will receive jobs from gitlab.com and it will start executing them.
